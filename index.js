class Card {
    constructor(authorName, authorEmail, title, text, postId) {
        this.authorName = authorName;
        this.authorEmail = authorEmail;
        this.title = title;
        this.text = text;
        this.postId = postId;
        this.div = document.createElement('div');
        this.deleteButton = document.createElement('button');
    }

    createElements() {
        this.div.className = 'card';
        this.div.innerHTML = `<a href="#" class="author-name">${this.authorName}</a>
                                <a href="mailto:${this.authorEmail}" class="author-email">${this.authorEmail}</a>
                                <h4 class="post-title">${this.title}</h4>
                                <p class="post-content">${this.text}</p>`
        this.deleteButton.innerText = 'Delete post';
        this.div.append(this.deleteButton);

        this.deleteButton.addEventListener('click', () => {

            fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
                method: 'DELETE',
            })
                .then(({status}) => {
                    if (status === 200) {
                        this.div.remove()
                    }
                })
        })
    }


    render(selector) {
        this.createElements();
        document.querySelector(selector).prepend(this.div);
    }


}


fetch('https://ajax.test-danit.com/api/json/users')
    .then((response) => {
            return response.json();
        }
    )
    .then((res) => {

            res.forEach(({id: userId, name, email}) => {

                fetch(`https://ajax.test-danit.com/api/json/users/${userId}/posts`)
                    .then((response) => {
                        return response.json();
                    })
                    .then((res) => {

                        res.forEach(({title, body, id: postID}) => {
                            const card = new Card(name, email, title, body, postID);
                            card.render(".container");
                        })
                    })
            })
        }
    )

